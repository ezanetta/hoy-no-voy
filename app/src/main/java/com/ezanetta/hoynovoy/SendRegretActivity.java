package com.ezanetta.hoynovoy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class SendRegretActivity extends Activity {

    @InjectView(R.id.regretInputText) EditText mRegretInputText;
    @InjectView(R.id.sendProgressBar) ProgressBar mProgressBar;
    @InjectView(R.id.sendRegretButton) Button mSendRegretButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_regret);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.sendRegretButton)
    public void sendRegret(){
        if(mRegretInputText.length() < 10){
            AlertMessage("Oops!", "Humano de pocas palabras, esforzate un poco más!", "ok",false);
        } else {
            String regret = mRegretInputText.getText().toString();
            showOrNotButtons(false);
            saveRegret(regret);
        }
    }

    public void showOrNotButtons(Boolean show){
        if(show == false){
            mSendRegretButton.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mSendRegretButton.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.backButton)
    public void back(){
        finish();
    }

    public void AlertMessage(String title, String message, String button, final Boolean main){
        /* DIALOG BUILDER */
        AlertDialog.Builder builder = new AlertDialog.Builder(SendRegretActivity.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(main == false) {
                    dialog.cancel();
                } else {
                    dialog.cancel();
                    finish();
                }
            }
        });
                /* DIALOG BUILDER */
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void saveRegret(String regret) {

        ParseObject bohfRegrets = new ParseObject("regrets");
        bohfRegrets.put("regret", regret);
        bohfRegrets.put("active",false);
        bohfRegrets.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                showOrNotButtons(true);
                if (e == null) {
                    String message = "Tu excusa será indexada en nuestra base de datos y evaluada por expertos en no ir a trabajar!";
                    AlertMessage("Muchas gracias", message, "ok", true);
                } else {
                    AlertMessage("Oops, algo fallo", "Por favor intentá mas tarde. Asegurate de tener conexion", "ok", false);
                }

            }
        });
    }
}
