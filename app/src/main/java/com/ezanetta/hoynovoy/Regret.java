package com.ezanetta.hoynovoy;

/**
 * Created by ezanetta on 5/4/15.
 */
public class Regret {
    private String mRegret;
    private String[] mHardcoreRegrets = {"Se me inundó la casa y tengo que esperar al plomero.",
            "Tengo que acompañar a mi abuela a hacer unos tramites.",
            "Me quede encerrado adentro de mi casa, tengo que esperar al cerrajero.",
            "Ayer a la noche comí algo que me cayó mal y me desperté con vómitos.",
            "Tengo al hijo de mi hermana con fiebre y no tiene con quien dejarlo.",
            "Mañana van a instalarme internet/cable y necesito estar en mi casa.",
            "Iba camino al trabajo y me robaron, fui a la comisaría a hacer la denuncia.",
            "Falleció mi abuela, voy a estar con mi familia todo el día.",
            "Tengo roto el baño y lo están arreglando, me tengo que quedar hasta que terminen.",
            "Tengo que ir al colegio a anotar a mi hijo y se están agotando las vacantes.",
            "Estoy con mucho dolor de cabeza, me dan puntadas todo el tiempo , si se me pasa voy.",
            "Tengo que llevar a mi gato al veterinario, no para de vomitar.",
            "Me levanté con diarrea, si se me pasa voy.",
            "Me llamaron de la inmobiliaria que necesitan que vaya urgente por un problema con el contrato.",
            "Tengo que hacer el tramite de la visa.",
            "Necesito ir a hacer el cambio de domicilio a una comisaría.",
            "Hoy no voy a poder ir, tengo un problema personal.",
            "Tuve un problema familiar grave, no puedo ir el día de hoy.",
            "Ayer mi papá se cayó de la escalera, y tengo que acompañarlo al medico hoy.",
            "Tengo mucho dolor de hígado , y solo se me pasa estando acostado.",
            "Estuve toda la noche con dolor de cabeza y mareos, no me siento nada bien.",
            "Estoy encerrado en el ascensor, si me rescatan voy.",
            "Cortaron el agua y sin bañarme no puedo ir"};

    public Regret(String regret){
        mRegret = regret;
    }

    public Regret(){}

    public String getRegret() {
        return mRegret;
    }

    public void setRegret(String regret) {
        mRegret = regret;
    }

    public String[] getHardcoreRegrets() {
        return mHardcoreRegrets;
    }

    public void setHardcoreRegrets(String[] hardcoreRegrets) {
        mHardcoreRegrets = hardcoreRegrets;
    }
}
