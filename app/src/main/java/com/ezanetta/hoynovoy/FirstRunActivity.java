package com.ezanetta.hoynovoy;

import android.app.Activity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class FirstRunActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_run);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.okButton)
    public void ok(){
        finish();
    }
}
