package com.ezanetta.hoynovoy;


import android.app.ListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class allRegretsActivity extends ListActivity {


    public static final String TAG = allRegretsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_regrets);
        ButterKnife.inject(this);

        Intent intent = getIntent();
        String[] allRegrets = intent.getStringArrayExtra(MainActivity.ALL_REGRETS);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.regret_list_item, R.id.listItem ,allRegrets);
        setListAdapter(adapter);

    }

    @OnClick(R.id.backButtonAll)
    public void back(){
        finish();
    }
    


}
