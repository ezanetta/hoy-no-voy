package com.ezanetta.hoynovoy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import io.fabric.sdk.android.Fabric;

import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends Activity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public Regret[] mRegrets;
    public String[] mHardcoreRegrets;
    public static final String ALL_REGRETS = "ALL_REGRETS";
    float x1,x2;
    float y1, y2;

    @InjectView(R.id.regretTextView) TextView mRegretTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        // INICIO PARSE
        Parse.initialize(this, "hNTvUmW3TOjauN2d7WbfJdjQQgCNvaZWqzhnUrTW", "HCoSKgSjfpUvU4tCfDel4yxUnYrnru2EBwPkJoOh");

        if(isFirstRun()){
            Intent intent = new Intent(MainActivity.this, FirstRunActivity.class);
            startActivity(intent);
        }

        if(isNetworkAvailable()){
            getRegrets();
        }

    }

    @OnClick(R.id.viewAllButton)
    public void toAllRegretsActivity(){
        Intent intent = new Intent(MainActivity.this, allRegretsActivity.class);

        if(mRegrets == null){
            Regret regretObj = new Regret();
            mHardcoreRegrets = regretObj.getHardcoreRegrets();
            intent.putExtra(ALL_REGRETS, mHardcoreRegrets);
        } else {
            String [] regrets = new String[mRegrets.length];
            for(int i = 0; i< mRegrets.length; i++){
                regrets[i] = mRegrets[i].getRegret();
            }
            intent.putExtra(ALL_REGRETS, regrets);
        }

        startActivity(intent);
    }

    @OnClick(R.id.sendRegret)
    public void toSendRegretActivity(){
        Intent intent = new Intent(MainActivity.this, SendRegretActivity.class);
        startActivity(intent);
    }

    public void newRegret(String side){

        if(mRegrets == null){
            Regret regretObj = new Regret();
            mHardcoreRegrets = regretObj.getHardcoreRegrets();
            int idx = new Random().nextInt(mHardcoreRegrets.length);
            String regret = mHardcoreRegrets[idx];

            animateText(side);
            mRegretTextView.setText(regret);

            // Vuelvo a pedir las excusas
            if(isNetworkAvailable()){
                getRegrets();
            }
        } else {
            int idx = new Random().nextInt(mRegrets.length);
            String regret = mRegrets[idx].getRegret();

            animateText(side);
            mRegretTextView.setText(regret);

        }

    }

    private void animateText(String side) {
        if(side.equals("left")){
            YoYo.with(Techniques.BounceInLeft)
                    .duration(700)
                    .playOn(mRegretTextView);
        } else {
            YoYo.with(Techniques.BounceInRight)
                    .duration(700)
                    .playOn(mRegretTextView);
        }
    }

    public void getRegrets() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("regrets");
        query.whereEqualTo("active", true);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    mRegrets = new Regret[list.size()];

                    for (int i = 0; i < list.size(); i++) {
                        String regretString = list.get(i).getString("regret");
                        Regret regret = new Regret(regretString);
                        mRegrets[i] = regret;
                    }

                } else {
                    Log.d("Regrets", "Error: " + e.getMessage());
                }
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()){
            isAvailable = true;
        }

        return isAvailable;
    }

    private boolean isFirstRun(){
        boolean isFirst = false;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted =  prefs.getBoolean(getString(R.string.pref_previously_started), false);

        if(!previouslyStarted){
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
            edit.commit();
            isFirst = true;
        }

        return isFirst;
    }

    public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction())
        {
            // when user first touches the screen we get x and y coordinate
            case MotionEvent.ACTION_DOWN:
            {
                x1 = touchevent.getX();
                y1 = touchevent.getY();
                break;
            }
            case MotionEvent.ACTION_UP:
            {
                x2 = touchevent.getX();
                y2 = touchevent.getY();

                //if left to right sweep event on screen
                if (x1 < x2) {
                    newRegret("left");
                }

                // if right to left sweep event on screen
                if (x1 > x2) {
                    newRegret("right");
                }
                break;
            }
        }
        return false;
    }

}
